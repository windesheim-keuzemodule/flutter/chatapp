import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

import '../widgets/chat/messages.dart';
import '../widgets/chat/new_message.dart';

class ChatScreen extends StatefulWidget {
  const ChatScreen({Key? key}) : super(key: key);

  @override
  State<ChatScreen> createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {

  @override
  void initState() {
    super.initState();

    // FirebaseMessaging.instance.getInitialMessage();
    // FirebaseMessaging.instance.getToken();
    // FirebaseMessaging.instance.subscribeToTopic('chat');

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      print('onMessage');
      print(message.notification!.title);
      print(message.notification!.body);
    });
    

    FirebaseMessaging.onBackgroundMessage((RemoteMessage message) async{
      print('onBackgroundMessage');
      print(message.notification);
      return;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Flutter Chat'),
        actions: [
          DropdownButton(
            underline: Container(),
            icon: Icon(
              Icons.more_vert,
              color: Theme.of(context).primaryIconTheme.color,
            ),
            items: [
              DropdownMenuItem(
                child: Row(
                  children: [
                    Icon(Icons.exit_to_app,
                        color: Theme.of(context).colorScheme.secondary),
                    Text(
                      'Logout',
                      style: TextStyle(
                          color: Theme.of(context).colorScheme.secondary),
                    ),
                  ],
                ),
                value: 'Logout',
              ),
            ],
            onChanged: (item) {
              if (item == 'Logout') {
                FirebaseAuth.instance.signOut();
              }
            },
          )
        ],
      ),
      body: Column(
        children: const [
          Expanded(
            child: Messages(),
          ),
          NewMessage(),
        ],
      ),
    );
  }
}
